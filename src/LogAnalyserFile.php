<?php
declare(strict_types=1);

namespace LogAnalyser;

use LogAnalyser\Log\SimpleLog;

class LogAnalyserFile implements LogAnalyserInterface
{
    /** @var string $logFilePath */
    private $logFilePath;

    /**
     * @param string $fileAbsoulutePath
     */
    public function __construct(string $fileAbsoulutePath)
    {
        $this->logFilePath = $fileAbsoulutePath;
    }

    /**
     * Return logs given newer than given period
     * 
     * @param \DateTime $period
     * @return array[SimpleLog]
     */
    public function analyse(\DateTime $period): array
    {
        return $this->readLogsFromFile($period);
    }

    /**
     * Remove logs older than given period
     * 
     * @param \DateTime $period
     * @return void
     */
    public function clear(\DateTime $period): void
    {
        $this->writeClearLogsToFile($period);
    }

    /**
     * @param \DateTime $period
     * @return SimpleLog
     * @throws \Exception
     */
    private function readLogsFromFile(\DateTime $period)
    {
        $logs = [];

        if (file_exists($this->logFilePath)) {
            $readStream = fopen($this->logFilePath, "r");

            while (($line = fgets($readStream)) !== false) {
                list($datetime, $type, $message) = $this->parseLogString($line);

                if ($datetime > $period) {
                    $logs[] = new SimpleLog($message, $type, $datetime);
                }
            }

            fclose($readStream);

        } else {
            throw new \Exception("Log file not found");
        }

        return $logs;
    }

    /**
     * Reads log file if entry is newer than given period add to temp log file
     * replace log file with temp at end of stream
     * @param \DateTime $peroid
     * @throws \Exception
     */
    private function writeClearLogsToFile(\DateTime $peroid)
    {
        $tempFile = sprintf("%s.tmp", $this->logFilePath);
        $writeStream = fopen($tempFile, "w");

        if (file_exists($this->logFilePath)) {
            $readStream = fopen($this->logFilePath, "r");

            while (($line = fgets($readStream)) !== false) {
                list($datetime) = $this->parseLogString($line);

                if ($datetime > $peroid) {
                    fwrite($writeStream, $line);
                }
            }

            fclose($readStream);
            fclose($writeStream);

            rename($tempFile, $this->logFilePath);

        } else {
            throw new \Exception("Log file not found");
        }
    }

    /**
     * @param string $line
     * @return type
     */
    private function parseLogString(string $line)
    {
        $array = explode(" ", $line);
        $dateString = implode(" ", [$array[0], $array[1]]);
        $datetime = new \DateTime($dateString);
        $type = $array[2];
        array_splice($array, 0, 2);
        $message = implode(" ", $array);

        return [$datetime, $type, $message];
    }
}
