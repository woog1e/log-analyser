<?php

declare(strict_types=1);

namespace LogAnalyser;

interface LogAnalyserInterface
{
    public function analyse(\DateTime $period): array;
    public function clear(\DateTime $period): void;
}
