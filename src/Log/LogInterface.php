<?php
declare(strict_types=1);

namespace LogAnalyser\Log;

interface LogInterface
{
    public function getMessage(): string;
    public function getType(): string;
    public function getDateTime(): \DateTime;
}
