<?php
declare(strict_types=1);

namespace LogAnalyser\Log;

class SimpleLog implements LogInterface
{

    /** @var string $message */
    private $message;
    /** @var string $type */
    private $type;
    /** @var \DateTime $datetime */
    private $datetime;

    public function __construct(string $message, string $type, \DateTime $datetime){
        $this->message = $message;
        $this->type = $type;
        $this->datetime = $datetime;
    }

    public function getMessage():string {
        return $this->message;
    }

    public function getType():string {
        return $this->type;
    }

    public function getDateTime():\DateTime {
        return $this->datetime;
    }
}
