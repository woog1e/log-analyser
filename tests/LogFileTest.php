<?php
declare(strict_types=1);

namespace LogAnalyser\Tests;

use PHPUnit\Framework\TestCase;
use LogAnalyser\LogAnalyserFile;

final class LogAnalyserFileTest extends TestCase
{
    public function setUp():void {
        copy(__DIR__."/resources/log.txt.test", __DIR__."/resources/log.txt");
    }

    public function tearDown():void {
        unlink(__DIR__."/resources/log.txt");
    }

    public function testAnalyse(): void
    {
     $analyser = new LogAnalyserFile(__DIR__."/resources/log.txt");

     $logs = $analyser->analyse(new \datetime("23-04-2023 15:00:00"));
     $this->assertEmpty($logs, "Should not return any logs");

     $logs = $analyser->analyse(new \datetime("23-04-2021 15:00:00"));
     $this->assertCount(6, $logs, "Should return all logs from file (6)");

     $logs = $analyser->analyse(new \datetime("23-04-2022 15:00:00"));
     $this->assertCount(4, $logs, "Should return 4 logs");
    }

    public function testClear(): void
    {
     $analyser = new LogAnalyserFile(__DIR__."/resources/log.txt");

     $analyser->clear(new \datetime("24-04-2022 15:00:00"));

     $lines = (file(__DIR__."/resources/log.txt"));
     $this->assertCount(3, $lines, "In log file should remain 3 lines of logs");
    }

}
